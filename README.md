# updateDVB_binaries

https://bitbucket.org/bluzee/updatedvb_binaries/downloads/

Deb packages for v4l-updatelee kernel and the updateDVB scanning program.

These should install into 64bit Mint 18 distros, Ubuntu 16.04 distro or
any Xenial or newer based distro. Newest version of updateDVB requires 
Mint 19 or Ubuntu Bionic 18.04 or newer.

**Warning**

If you are using any DKMS drivers such as nVidia proprietary drivers these may
need to be re-installed after installing the new kernel. If the udl kernel version 
is newer than your currently installed kernel please ensure there are drivers
available for the newer kernel before proceeding. It is highly recommended to 
switch to the open source nouveau driver before rebooting. If the nvidia driver fails
to load reboot and select your old kernel to boot from the GRUB menu. The udl 
kernel packages can be safely removed. You will not be able to use updateDVB without
the patched kernel drivers installed however.

Download the linux-headers linux-image and linux-libc-dev packages
into a seperate directory and install with...

sudo dpkg -i *.deb

Reboot. You should now be running the newly installed kernel.
Check with

uname -a

If not, reboot again and select the udl kernel from grub menu. 

Download the updateDVB deb file.  Use *gdebi* to install it as it will require
a number of QT5 packages to be installed as dependencies.  

Some DVB devices may need firmware to be installed into /lib/firmware before
they will work if you have not already done so. 

You should now be able to run updateDVB and spectrum scan with your supported DVB
device. See https://gitlab.com/updatelee/updateDVB

